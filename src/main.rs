extern crate anyhow;

use rust_bert::{pipelines::conversation::{
    ConversationConfig, ConversationManager, ConversationModel,
}, gpt2::{Gpt2ModelResources, Gpt2VocabResources, Gpt2MergesResources, Gpt2ConfigResources}, resources::RemoteResource, pipelines::common::ModelType};

fn main() -> anyhow::Result<()> {
    let config = ConversationConfig {
        do_sample: false,
        num_beams: 3,
        model_type: ModelType::GPT2,
        model_resource: Box::new(RemoteResource::from_pretrained(
                Gpt2ModelResources::DIALOGPT_SMALL,
        )),
        config_resource: Box::new(RemoteResource::from_pretrained(
                Gpt2ConfigResources::DIALOGPT_SMALL,
        )),
        vocab_resource: Box::new(RemoteResource::from_pretrained(
                Gpt2VocabResources::DIALOGPT_SMALL,
        )),
        merges_resource: Some(Box::new(RemoteResource::from_pretrained(
                    Gpt2MergesResources::DIALOGPT_SMALL,
        ))),
        ..Default::default()
    };
    let conversation_model = ConversationModel::new(config)?;
    let mut conversation_manager = ConversationManager::new();

    let conversation_1_id =
        conversation_manager.create("Going to the movies tonight - any suggestions?");
    let _conversation_2_id = conversation_manager.create("What's the last book you have read?");

    let output = conversation_model.generate_responses(&mut conversation_manager);

    println!("{:?}", output);

    let _ = conversation_manager
        .get(&conversation_1_id)
        .unwrap()
        .add_user_input("Is it an action movie?");

    let output = conversation_model.generate_responses(&mut conversation_manager);

    println!("{:?}", output);

    let output = conversation_model.generate_responses(&mut conversation_manager);

    println!("{:?}", output);

    Ok(())
}
